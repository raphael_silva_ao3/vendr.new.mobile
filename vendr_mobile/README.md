# vendr_mobile

Projeto mobile do Vendr.

## **Requisitos mínimos para rodar o projeto**
Flutter: https://docs.flutter.dev/get-started/install

</br>


</br></br>

## **Como compilar localmente via script**

O projeto roda em cima de pipelines provisionadas no AzureDevOps, porém, em caso de contingência, existe um script local versionado neste projeto que pode ser utilizado.

O arquivo fica localizado em **./ci/sh/exec_fastlane_local.sh**

Este script tem o intuito de injetar variáveis de ambiente e chamar comandos que executam lanes do Fastlane (que é feito em cima da tecnologia Ruby), gerar os binários de Android e iOS e distribuir automaticamente os mesmos para o AppDistribution do projeto do Firebase Vendr.

</br>

### **Requisitos mínimos para compilar localmente + distribuir no Firebase**

* Ruby: https://www.ruby-lang.org/en/documentation/installation/#managers

* Fastlane: https://docs.fastlane.tools/

* Firebase CLI: https://firebase.google.com/docs/cli#install_the_firebase_cli

</br></br>


### **Configuração**
Para compilar localmente via script sh, você deverá apenas informar os valores nas variáveis de ambiente.

Para isso, abra o arquivo **./ci/sh/exec_fastlane_local.sh** e informe os seguintes valores:

```

export FIREBASE_TOKEN=SUA_CREDENCIAL_FIREBASE_CLI

export GIT_BASIC_AUTHORIZATION=SEU_USER_REPO_CREDENTIALS:SUA_CHAVE_APP_PASSWORD_REPO_CREDENTIALS. => Ex: usuario_x:12AVB232

export MATCH_PASSWORD=SENHA_MATCH

export LOCAL_APPLE_P8=PATH_ARQUIVO_AUTH_KEY_APPLE.p8

```
> **Para usuários de MAC:** Pegar tanto o arquivo AppleApiKey.p8 quanto o Math Password com alguém da equipe, pois são dados sensíveis.

</br></br>
## **Firebase**

Este spript, após executado e gerados os binários de Android e iOS, distribui os mesmos para o AppDistribution no projeto Firebase do Vendr.

Para isto funcionar, deveremos gerar um Firebase Token com o seu usuário.

> **Observação:** Seu usuário deve estar dentro dos users do projeto do Firebase, e não dentro do App Distribution, pois são coisas distintas.

</br>

Comando para gerar o FirebaseToken:

```
firebase login:ci
```
</br>

Isto irá gerar uma chave, que devemos alimentar em nossa variável de ambiente.

```
FIREBASE_TOKEN=1//0aAAAAAAAAA-BBBBBBBBBBBBBBBBBBBBBBBbbbbBBB
```

</br></br>

> **ATENÇÃO: OS PASSOS ABAIXO SÃO NECESSÁRIOS PARA COMPILAÇÃO DO APLICATIVO PARA iOS**

</br>

### **Repositório de credenciais**
Para iOS, você deverá primeiramente criar um app password no repositório de credenciais

* **Repositório:** https://bitbucket.org/xxxxx/vendr.new.mobile.credentials.git

* **Como criar a key (app password):** https://support.atlassian.com/bitbucket-cloud/docs/app-passwords/

</br>

Isto irá gerar uma key secret, que devemos alimentar em nossa variável de ambiente.

```
GIT_BASIC_AUTHORIZATION={user_repo_credentials:app_password_key_secet_gerada}

Exemplo:
GIT_BASIC_AUTHORIZATION=raphaelsilva:AbCDeFgH1234
```

</br></br>

### **Match**
Primeiramente, teremos que alimentar a variável de ambiente **MATCH_PASSWORD** com um valor (pegar este valor com alguém da equipe, pois é um dado sensível).

```
MATCH_PASSWORD=SENHA
```

Após isto, deveremos primeiramente ir para a pasta ios do projeto, e depois rodar o comando "match" do fastlane, afim de baixar os certificados match que estão no repositório de credentials.

Comando: 
```
cd ios;

bundle exec fastlane match development -a com.iob.vendr.mobile,com.iob.vendr.mobile.dev,com.iob.vendr.mobile.prerc,com.iob.vendr.mobile.rc --readonly --verbose

```

> Observação: se você estiver em um MacOS novo pela primeira vez, retire a flag --readonly

</br></br>



### **Rodando o script**
Por fim, após todas as variáveis de ambientes preenchidas, basta rodar o script.

```

sh exec_fastlane_local.sh

```